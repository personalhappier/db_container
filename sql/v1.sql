-- V1__initial_schema.sql

-- Create the schema
CREATE SCHEMA IF NOT EXISTS public;

-- Create the users table
CREATE TABLE public.users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(50) NOT NULL UNIQUE,
    email VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- Create the roles table
CREATE TABLE public.roles (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE,
    description TEXT
);

-- Create the user_roles table to establish many-to-many relationship between users and roles
CREATE TABLE public.user_roles (
    user_id INT NOT NULL,
    role_id INT NOT NULL,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY (user_id) REFERENCES public.users (id) ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES public.roles (id) ON DELETE CASCADE
);

-- Insert initial roles
INSERT INTO public.roles (name, description) VALUES ('admin', 'Administrator role');
INSERT INTO public.roles (name, description) VALUES ('user', 'Regular user role');
