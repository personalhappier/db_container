image: docker:latest

services:
  - name: docker:dind
    alias: docker

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

stages:
  - build
  - test
  - deploy

# Define the job to build the custom Docker image with Flyway and PostgreSQL client
build_image:
  stage: build
  script:
    - docker build -t my-flyway-image .

# Define the job to run PostgreSQL and Flyway migrations
flyway_migration:
  stage: test
  services:
    - name: postgres:latest
      alias: db
  variables:
    POSTGRES_DB: testdb
    POSTGRES_USER: testuser
    POSTGRES_PASSWORD: testpassword
    POSTGRES_HOST_AUTH_METHOD: trust
    DOCKER_HOST: tcp://docker:2375
  before_script:
    - apk update && apk add postgresql-client
  script:
    # Wait for PostgreSQL to be ready
    - until pg_isready -h db -U $POSTGRES_USER; do echo "Waiting for database to be ready..."; sleep 1; done
    # Run Flyway migrations
    - docker run --rm -v $(pwd)/sql:/flyway/sql --network host my-flyway-image -url=jdbc:postgresql://db:5432/$POSTGRES_DB -user=$POSTGRES_USER -password=$POSTGRES_PASSWORD migrate
